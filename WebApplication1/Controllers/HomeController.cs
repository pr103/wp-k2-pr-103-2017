﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Dictionary<string, Vozilo> vozila = (Dictionary<string, Vozilo>)HttpContext.Application["Vozila"];
           

            Dictionary<string, Korisnik> korisnici = (Dictionary<string, Korisnik>)HttpContext.Application["Korisnici"];


            Korisnik k = (Korisnik)Session["Korisnik"];
            Session["Vozila"] = vozila;

            if (k==null)
            {
                ViewBag.svavozila = vozila.Values;
                return View("Index");
                //return View((IEnumerable<string>)vozila);
            }

            
            ViewBag.korisnici = k;
            ViewBag.svavozila = vozila.Values;
            return View();

        }
        [HttpPost]
        public ActionResult SortModelAsc()
        {
            
            Dictionary<string, Vozilo> vozila = (Dictionary<string, Vozilo>)HttpContext.Application["Vozila"];

            vozila=Baza.SortASCModel(vozila);
            Session["Vozila"] = vozila;
           

            Dictionary<string, Korisnik> korisnici = (Dictionary<string, Korisnik>)HttpContext.Application["Korisnici"];


            Korisnik k = (Korisnik)Session["Korisnik"];
        

            if (k == null)
            {
                ViewBag.svavozila = vozila.Values;
                return View("Index");
                //return View((IEnumerable<string>)vozila);
            }


            ViewBag.korisnici = k;
            ViewBag.svavozila = vozila.Values;

            return View("Index");
             //return View("SortModelAsc",vozila);
        }
        [HttpPost]
        public ActionResult SortModelDesc()
        {

            Dictionary<string, Vozilo> vozila = (Dictionary<string, Vozilo>)HttpContext.Application["Vozila"];

            vozila=Baza.SortDESCModel(vozila);
            Session["Vozila"] = vozila;

            Dictionary<string, Korisnik> korisnici = (Dictionary<string, Korisnik>)HttpContext.Application["Korisnici"];


            Korisnik k = (Korisnik)Session["Korisnik"];


            if (k == null)
            {
                ViewBag.svavozila = vozila.Values;
                return View("Index");
                //return View((IEnumerable<string>)vozila);
            }


            ViewBag.korisnici = k;
            ViewBag.svavozila = vozila.Values;

            return View("Index");
            //return View("SortModelDesc", vozila);
        }
        [HttpPost]
        public ActionResult SortMarkaDesc()
        {

            Dictionary<string, Vozilo> vozila = (Dictionary<string, Vozilo>)HttpContext.Application["Vozila"];

            vozila = Baza.SortMarkaDesc(vozila);
            Session["Vozila"] = vozila;
            Dictionary<string, Korisnik> korisnici = (Dictionary<string, Korisnik>)HttpContext.Application["Korisnici"];


            Korisnik k = (Korisnik)Session["Korisnik"];


            if (k == null)
            {
                ViewBag.svavozila = vozila.Values;
                return View("Index");
                //return View((IEnumerable<string>)vozila);
            }


            ViewBag.korisnici = k;
            ViewBag.svavozila = vozila.Values;

            return View("Index");
            //return View("SortModelDesc", vozila);
        }
        [HttpPost]
        public ActionResult SortMarkaAsc()
        {

            Dictionary<string, Vozilo> vozila = (Dictionary<string, Vozilo>)HttpContext.Application["Vozila"];

            vozila = Baza.sortMarkaAsc(vozila);
            Session["Vozila"] = vozila;
            Dictionary<string, Korisnik> korisnici = (Dictionary<string, Korisnik>)HttpContext.Application["Korisnici"];


            Korisnik k = (Korisnik)Session["Korisnik"];


            if (k == null)
            {
                ViewBag.svavozila = vozila.Values;
                return View("Index");
                //return View((IEnumerable<string>)vozila);
            }


            ViewBag.korisnici = k;
            ViewBag.svavozila = vozila.Values;

            return View("Index");
            //return View("SortModelDesc", vozila);
        }
        [HttpPost]
        public ActionResult SortCenaAsc()
        {

            Dictionary<string, Vozilo> vozila = (Dictionary<string, Vozilo>)HttpContext.Application["Vozila"];

            vozila = Baza.SortCenaAsc(vozila);
            Session["Vozila"] = vozila;
            Dictionary<string, Korisnik> korisnici = (Dictionary<string, Korisnik>)HttpContext.Application["Korisnici"];


            Korisnik k = (Korisnik)Session["Korisnik"];


            if (k == null)
            {
                ViewBag.svavozila = vozila.Values;
                return View("Index");
                //return View((IEnumerable<string>)vozila);
            }


            ViewBag.korisnici = k;
            ViewBag.svavozila = vozila.Values;

            return View("Index");
            //return View("SortModelDesc", vozila);
        }
        [HttpPost]
        public ActionResult SortCenaDesc()
        {

            Dictionary<string, Vozilo> vozila = (Dictionary<string, Vozilo>)HttpContext.Application["Vozila"];

            vozila = Baza.SortCenaDesc(vozila);
            Session["Vozila"] = vozila;
            Dictionary<string, Korisnik> korisnici = (Dictionary<string, Korisnik>)HttpContext.Application["Korisnici"];


            Korisnik k = (Korisnik)Session["Korisnik"];


            if (k == null)
            {
                ViewBag.svavozila = vozila.Values;
                return View("Index");
                //return View((IEnumerable<string>)vozila);
            }


            ViewBag.korisnici = k;
            ViewBag.svavozila = vozila.Values;

            return View("Index");
            //return View("SortModelDesc", vozila);
        }
        [HttpPost]
        public ActionResult Range(int from,int to)
        {

            Dictionary<string, Vozilo> vozila = (Dictionary<string, Vozilo>)HttpContext.Application["Vozila"];

            vozila = Baza.Range(vozila,from,to);
            Session["Vozila"] = vozila;
            Dictionary<string, Korisnik> korisnici = (Dictionary<string, Korisnik>)HttpContext.Application["Korisnici"];


            Korisnik k = (Korisnik)Session["Korisnik"];


            if (k == null)
            {
                ViewBag.svavozila = vozila.Values;
                return View("Index");
                //return View((IEnumerable<string>)vozila);
            }


            ViewBag.korisnici = k;
            ViewBag.svavozila = vozila.Values;

            return View("Index");
            //return View("SortModelDesc", vozila);
        }
        [HttpPost]
        public ActionResult Search(string search,string itemOfSearch)
        {

            Dictionary<string, Vozilo> vozila = (Dictionary<string, Vozilo>)HttpContext.Application["Vozila"];

            vozila = Baza.Search(vozila, search, itemOfSearch);
            Session["Vozila"] = vozila;
            Dictionary<string, Korisnik> korisnici = (Dictionary<string, Korisnik>)HttpContext.Application["Korisnici"];


            Korisnik k = (Korisnik)Session["Korisnik"];


            if (k == null)
            {
                ViewBag.svavozila = vozila.Values;
                return View("Index");
                //return View((IEnumerable<string>)vozila);
            }


            ViewBag.korisnici = k;
            ViewBag.svavozila = vozila.Values;

            return View("Index");
            //return View("SortModelDesc", vozila);
        }
    }
}