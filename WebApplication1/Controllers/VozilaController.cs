﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class VozilaController : Controller
    {
        // GET: Vozila
        public ActionResult Index(string id)
        {
            Dictionary<string, Vozilo> vozila = (Dictionary<string, Vozilo>)HttpContext.Application["Vozila"];

            ViewBag.Vozila= vozila[id];

            if (vozila==null)
            {
                ViewBag.Message = "Car isn't availabe";
            }

            return View();
        }
        public ActionResult AddCar()
        {
            Vozilo vozilo = new Vozilo();
            Session["vozilo"] = vozilo;
            return View(vozilo);
        }
        // GET: Vozila/AddCar
        [HttpPost]
        public ActionResult AddCar(Vozilo v)
        {
            Dictionary<string, Vozilo> vozilo = (Dictionary<string, Vozilo>)HttpContext.Application["Vozila"];

            //readonly ne moze se promeniti ali eto
            if (vozilo.ContainsKey(v.Sasija))
            {
                ViewBag.Message = "Car is already registered!";
                return View();
            }
            if (v.Model.Length<3)
            {
                ViewBag.Message = "Car model should be longer than 3!";
                return View();
            }
            if (v.Marka.Length < 3)
            {
                ViewBag.Message = "Car brand name should be longer than 3!";
                return View();
            }
            if (v.Marka.Length<3 && Baza.isAlphaNumeric(v.Marka)
                && Baza.isAlphaNumeric(v.Model)
                && string.IsNullOrWhiteSpace(v.Marka)
                && string.IsNullOrWhiteSpace(v.Model))
            {
                ViewBag.Message = "Car brand name should be longer than 3!";
                return View();
            }


            if (vozilo == null)
            {
                ViewBag.Message = "Car is not added to the list!";
                return View();
            }

            Session["vozilo"] = vozilo;
            vozilo.Add(v.Sasija, v);

            
            Baza.SaveCar(v);

            return RedirectToAction("Index", "Home");



        }
        // GET Vozila/GetCars
        public ActionResult GetCars()
        {

            Dictionary<string, Vozilo> vozila = (Dictionary<string, Vozilo>)HttpContext.Application["Vozila"];
            


            ViewBag.svavozila = vozila.Values;
           
            return View();
        }

        // GET Vozila/Delete
        public ActionResult Delete(string id)
        {

            Dictionary<string, Vozilo> vozila = (Dictionary<string, Vozilo>)HttpContext.Application["Vozila"];

            vozila.Remove(id);

            ViewBag.Vozila = vozila.Values;

            return RedirectToAction("GetCars", "Vozila");

        }
        // GET Vozila/Delete

        [HttpPost]
        public ActionResult Edit(Vozilo v)
        {


            Dictionary<string, Vozilo> vozila = (Dictionary<string, Vozilo>)HttpContext.Application["Vozila"];

            vozila[v.Sasija] = v;

            if (v.Marka.Length < 3)
            {
                ViewBag.Message = "Car brand name should be longer than 3!";
                //return View($"Edit?id={v.Sasija}");
                return RedirectToAction("Index", "Home");
            }
            if (v.Marka.Length < 3 && Baza.isAlphaNumeric(v.Marka)
                && Baza.isAlphaNumeric(v.Model) 
                && string.IsNullOrWhiteSpace(v.Marka)
                && string.IsNullOrWhiteSpace(v.Model))
            {
                ViewBag.Message = "Car brand name should be longer than 3! Model must be entered!";
                // return View($"Edit?id={v.Sasija}");
                return RedirectToAction("Index", "Home");
            }

            Baza.UpdateCar(vozila);

            return RedirectToAction("Index","Home");




        }
        public ActionResult Edit(string id)
        {
            Dictionary<string, Vozilo> vozila = (Dictionary<string, Vozilo>)HttpContext.Application["Vozila"];


            ViewBag.Vozila = vozila[id];

            return View();
        }
            // GET Vozila/Sort
        


        // GET Vozila/Delete
        //public ActionResult Sort(string Sorting_Order)
        //{
        //    Dictionary<string, Vozilo> vozila = (Dictionary<string, Vozilo>)HttpContext.Application["Vozila"];

        //    ViewBag.SortingName = String.IsNullOrEmpty(Sorting_Order) ? "Name_Description" : "";
        //    ViewBag.SortingDate = Sorting_Order == "Date_Enroll" ? "Date_Description" : "Date";



        //    switch (Sorting_Order)
        //    {
        //        case "Name_Description":
        //            students = students.OrderByDescending(stu => stu.FirstName);
        //            break;
        //        case "Date_Enroll":
        //            students = students.OrderBy(stu => stu.EnrollmentDate);
        //            break;
        //        case "Date_Description":
        //            students = students.OrderByDescending(stu => stu.EnrollmentDate);
        //            break;
        //        default:
        //            students = students.OrderBy(stu => stu.FirstName);
        //            break;
        //    }
        //    return View(students.ToList());



        //}

    }
}