﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class AutentifikacijaController : Controller
    {
        // GET: Autentifikacija
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult Registracija()
        {
            Korisnik korisnik = new Korisnik();
            Session["user"] = korisnik;
            return View(korisnik);
        }
   
        [HttpPost]
        public ActionResult Registracija(Korisnik korisnik)
        {
           

           Dictionary<string,Korisnik> korisnici = (Dictionary<string, Korisnik>)HttpContext.Application["Korisnici"];

            if (korisnici.ContainsKey(korisnik.Username))
            {
                ViewBag.Message = $"User with {korisnik.Username} already exists!";
                return View("Registracija");
            }
            if (korisnik.Username.Length<3)
            {
                ViewBag.Message = $"{korisnik.Username} is too short! Please put minimal lenght of 3 characters.";
                return View("Registracija");
            }
            if (korisnik.Password.Length<8)
            {
                ViewBag.Message = $"Password {korisnik.Password} is too short!Please put minimal lenght of 8 characters";
                return View("Registracija");
            }
            if (Baza.isAlphaNumeric(korisnik.Username))
            {
                ViewBag.Message = $"Username {korisnik.Username} is not in the right format!Please put a-z or A-Z or 0-9 characters ";
                return View("Registracija");
            }

            korisnici.Add(korisnik.Username,korisnik);
          

            Session["korisnik"] = korisnik;
            korisnik.Uloga = Korisnik_Uloga.KUPAC;
           
            Baza.SaveUser(korisnik);

            return RedirectToAction("Index", "Autentifikacija");
        }

        [HttpPost]
        public ActionResult Login(string username, string password)
        {

            Dictionary<string, Korisnik> korisnici = (Dictionary<string, Korisnik>)HttpContext.Application["Korisnici"];
            Dictionary<string, Kupovina> korpa = (Dictionary<string, Kupovina>)HttpContext.Application["Korpa"];

            Korisnik k=new Korisnik();
            Kupovina k1 = new Kupovina();

            foreach (Korisnik korisnik in korisnici.Values)
            {
                if (korisnici.ContainsKey(username) && korisnici[username].Password.Equals(password))
                {
                  
                        k = korisnici[username];
                        Session["korisnik"] = k;



                }
                else
                {
                    //ViewBag.Message = $"User {username} with credentials does not exist!";
                      
                    
                    ViewBag.Message = "Your username/password isn't good!";
                    return View("Index");
                    
                }

            }
          
           
            return RedirectToAction("Index", "Home");
        }
        
        public ActionResult Logout()
        {
            Session["korisnik"] = null;
         


            return RedirectToAction("Index", "Home");
        }
        public ActionResult GetUsers()
        {
          
            Dictionary<string, Korisnik> korisnici = (Dictionary<string, Korisnik>)HttpContext.Application["Korisnici"];

            
            ViewBag.Korisnici = korisnici.Values;
            return View();
     
        }
        public ActionResult Delete(string id)
        {

            Dictionary<string, Korisnik> korisnici = (Dictionary<string, Korisnik>)HttpContext.Application["Korisnici"];

            korisnici.Remove(id);

            ViewBag.Korisnici = korisnici.Values;

            return RedirectToAction("GetUsers", "Autentifikacija");

        }
        
       
    }
}