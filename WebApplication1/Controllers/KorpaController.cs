﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using WebApplication1.Models;

namespace WebApplication1.Controllers
{
    public class KorpaController : Controller
    {
        // GET: Korpa
        //Radi kao funkcija Buy()
        public ActionResult Index(string id)
        {
            
                Dictionary<string, Vozilo> vozilo = (Dictionary<string, Vozilo>)HttpContext.Application["Vozila"];
                Dictionary<string, Kupovina> korpa = (Dictionary<string, Kupovina>)HttpContext.Application["Korpa"];

            //foreach (var item in korpa.Values)
            //{
            //    if (item.Kupac.Equals(k))
            //    {

            //    }
            //}
                ViewBag.Vozila = vozilo[id];

                Korisnik k = (Korisnik)Session["Korisnik"];

               vozilo[id].Status = false;
               vozilo[id].Korisnik = k.Username;
               Kupovina kupovina = new Kupovina(k, vozilo[id],vozilo[id].Cena+200);
               
               Session["Korpa"]=kupovina;

            try
            {
                korpa.Add(id, kupovina);
            }
            catch (Exception)
            {
                ViewBag.Message = "You bought already this car";

                return View("Index");
            }
                
            
               if (k == null)
                {
                    ViewBag.Message = "You must log in to buy a car";

                    return View("Index");
                }

                if (vozilo == null)
                {
                    ViewBag.Message = "Car isn't availabe for sale or does not exists!";

                    return View("Index");
                }

              

                ViewBag.Message = "You bought a car, congrats!";

                 
                 Baza.SaveBoughtItem(kupovina);
                 Baza.UpdateCar(vozilo);
                ViewBag.Korpa = korpa.Values;

                return RedirectToAction("GetItems","Korpa");
            
        }
        public ActionResult GetItems()
        {
           
            Dictionary<string, Kupovina> korpa = (Dictionary<string, Kupovina>)HttpContext.Application["Korpa"];
            List<Kupovina> korisnikKupovine = new List<Kupovina>();

            Korisnik k = (Korisnik)Session["Korisnik"];


           
            foreach (var proizvodi in korpa)
            {
                if (k.Username.Equals(proizvodi.Value.Kupac.Username))
                {
                    korisnikKupovine.Add(proizvodi.Value);
                }
            }
        
            
            ViewBag.Korpa = korisnikKupovine;
            return View();

        }

    }
}