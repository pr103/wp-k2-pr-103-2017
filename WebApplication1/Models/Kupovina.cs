﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{

    public class Kupovina
    {
        Dictionary<string, Korisnik> korpa = new Dictionary<string, Korisnik>();

        private Korisnik kupac;

        public Korisnik Kupac
        {
            get { return kupac; }
            set { kupac = value; }
        }

        private Vozilo kupljeno;

        public Vozilo Kupljeno
        {
            get { return kupljeno; }
            set { kupljeno = value; }
        }

        private DateTime datumKupovine;     

        public DateTime DatumKupovine
        {
            get { return datumKupovine; }
            set { datumKupovine = value; }
        }

        private int naplacenaCena;

        public int NaplacenaCena
        {
            get { return naplacenaCena; }
            set { naplacenaCena = value; }
        }

        public Kupovina()
        {
           
        }

        public Kupovina(string username,string name,string lastname, string model, string marka,string sasija, int cena,DateTime datum,bool status)
        {
            Kupac = new Korisnik();
            Kupljeno = new Vozilo();
            Kupac.Username = username;
            Kupac.Name = name;
            Kupac.Lastname = lastname;
            Kupljeno.Model = model;
            Kupljeno.Marka = marka;
            Kupljeno.Sasija = sasija;
            NaplacenaCena = cena + 200;
            Kupljeno.Cena =NaplacenaCena;
            datumKupovine = datum;
            Kupljeno.Status = status;
         
        }
        public Kupovina(Korisnik korisnik, Vozilo vozilo,int naplacenaCena)
        {

            this.kupac = korisnik;
            this.kupljeno = vozilo;
            datumKupovine = DateTime.Now;
            this.naplacenaCena = naplacenaCena;
        }
       
    }
}