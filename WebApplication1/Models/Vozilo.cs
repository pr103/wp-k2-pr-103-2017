﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Vozilo
    {
        private Gorivo vrstaGoriva;

        public Gorivo VrstaGoriva
        {
            get { return vrstaGoriva; }
            set { vrstaGoriva = value; }
        }

        //Marka(minimalno 3 karaktera)
        private string marka;

        public string Marka
        {
            get { return marka; }
            set { marka = value; }
        }

        private string model;

        public string Model
        {
            get { return model; }
            set { model = value; }
        }

        private string sasija;

        public string Sasija
        {
            get { return sasija; }
            set { sasija = value; }
        }

        private string boja;

        public string Boja
        {
            get { return boja; }
            set { boja = value; }
        }

        private int brVrata;

        public int BrojVrata
        {
            get { return brVrata; }
            set { brVrata = value; }
        }

        private string description;

        public string Description
        {
            get { return description; }
            set { description = value; }
        }


        private int cena;

        public int Cena
        {
            get { return cena; }
            set { cena = value; }
        }

        // Na stanju(true/false)
        private bool status;

        public bool Status
        {
            get { return status; }
            set { status = value; }
        }
        private string korisnik;

        public string Korisnik
        {
            get { return korisnik; }
            set { korisnik = value; }
        }


        public Vozilo(string marka, string model, string sasija, string boja, int brVrata, Gorivo vrstaGoriva, string description, int cena, bool status,string korisnik) : this()
        {

            this.marka = marka;
            this.model = model;
            this.sasija = sasija;
            this.boja = boja;
            this.brVrata = brVrata;
            this.vrstaGoriva = vrstaGoriva;
            this.description = description;
            this.cena = cena;
            this.status = status;
            this.korisnik = korisnik;
        }

   
        public Vozilo()
        {
            status = true;
        }
    }
}