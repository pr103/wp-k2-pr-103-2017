﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Hosting;

namespace WebApplication1.Models
{
    public class Baza
    {
        public static Dictionary<string, Vozilo> ReadCars(string path)
        {
            Dictionary<string, Vozilo> vozilos = new Dictionary<string, Vozilo>();

            path = HostingEnvironment.MapPath(path);
            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);

            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] tokens = line.Split('|');
                Vozilo v = new Vozilo(
                    tokens[0], //mazda
                    tokens[1], //cx
                    tokens[2], //1
                    tokens[3], //crvena
                    int.Parse(tokens[4]),//4
                    (Gorivo)Enum.Parse(typeof(Gorivo), tokens[5]),//dizel
                    tokens[6],//zamena
                    int.Parse(tokens[7]),//25660
                    bool.Parse(tokens[8]),
                    tokens[9]);//true

                vozilos.Add(v.Sasija, v);
            }
            sr.Close();
            stream.Close();

            return vozilos;
        }

        public static Dictionary<string, Korisnik> ReadUsers(string path)
        {
            Dictionary<string, Korisnik> korisnici = new Dictionary<string, Korisnik>();

            path = HostingEnvironment.MapPath(path);

            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);

            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] tokens = line.Split('|');
                //  Korisnik k1 = new Korisnik((Korisnik_Uloga)Enum.Parse(typeof(Korisnik_Uloga), tokens[0]), tokens[1], tokens[2], tokens[3], tokens[4],tokens[5],tokens[6],);

                Korisnik k1 = new Korisnik(
                    tokens[0],
                    tokens[1],
                    tokens[2],
                    tokens[3],
                    tokens[4],
                    tokens[5],
                    tokens[6],
                    (Korisnik_Uloga)Enum.Parse(typeof(Korisnik_Uloga), tokens[7]));
                korisnici.Add(k1.Username, k1);
            }
            sr.Close();
            stream.Close();

            return korisnici;
        }

        public static Dictionary<string, Kupovina> ReadItems(string path)
        {
            Dictionary<string, Kupovina> korpa = new Dictionary<string, Kupovina>();

            path = HostingEnvironment.MapPath(path);

            FileStream stream = new FileStream(path, FileMode.Open);
            StreamReader sr = new StreamReader(stream);

            string line = "";
            while ((line = sr.ReadLine()) != null)
            {
                string[] tokens = line.Split('|');

                //joka|Jovana|Vujanic|Audi|CD12|sasija|26000|2000-03-02|true/false
                Kupovina k1 = new Kupovina(
                    tokens[0],//username
                    tokens[1],//name
                    tokens[2],//lastname
                    tokens[3], //marka
                    tokens[4], //model
                    tokens[5],//sasija
                    Int32.Parse(tokens[6]),//cena
                    DateTime.Parse(tokens[7]),//datum
                    bool.Parse(tokens[8]));//status

                korpa.Add(k1.Kupljeno.Sasija, k1);

            }
            sr.Close();
            stream.Close();

            return korpa;
        }
        public static void SaveUser(Korisnik korisnik)
        {
            // save user in file users.txt

            string k = "\n" + korisnik.Username + "|" + korisnik.Password + "|" + korisnik.Name + "|" +
                korisnik.Lastname + "|" + korisnik.Date + "|" + korisnik.Email + "|" +
                korisnik.Gender + "|" + korisnik.Uloga;

            string path = HostingEnvironment.MapPath("~/App_Data/korisnici.txt");

            //FileStream stream = new FileStream(path, FileMode.Append);

            //using (StreamWriter writetext = new StreamWriter(path))
            //{
            //    writetext.WriteLine(k);
            //}

            File.AppendAllText(path, k);
        }

        public static void SaveCar(Vozilo vozilo)
        {
            // save user in file users.txt
            //Mazda|CX30D116|1|Crvena|4|Dizel|ZamenaNE|26550|true
            string k = vozilo.Marka + "|" + vozilo.Model + "|" + vozilo.Sasija + "|" +
                vozilo.Boja + "|" + vozilo.BrojVrata + "|" + vozilo.VrstaGoriva + "|" + vozilo.Description + "|" + vozilo.Cena + "|"
                + string.Format(vozilo.Status.ToString()).ToLower() + "|" + vozilo.Korisnik + "\n";


            File.AppendAllText(HostingEnvironment.MapPath("~/App_Data/automobili.txt"), k);
        }

        public static void SaveBoughtItem(Kupovina korpa)
        {
            //joka|Jovana|Vujanic|Audi|CD12|sasija|26000|2000-03-02|true/false
            string k =
                korpa.Kupac.Username +
                "|" + korpa.Kupac.Name +
                "|" + korpa.Kupac.Lastname +
                "|" + korpa.Kupljeno.Model +
                "|" + korpa.Kupljeno.Marka +
                "|" + korpa.Kupljeno.Sasija +
                "|" + korpa.NaplacenaCena +
                "|" + korpa.DatumKupovine.ToString() +
                "|" + string.Format(korpa.Kupljeno.Status.ToString()).ToLower() + Environment.NewLine;


            File.AppendAllText(HostingEnvironment.MapPath("~/App_Data/kupacKupovine.txt"), k);
        }
        #region Logika za Sort
        public static Dictionary<string, Vozilo> SortASCModel(Dictionary<string, Vozilo> v)
        {

            // string path = "~/App_Data/automobili.txt";
            //path = HostingEnvironment.MapPath(path);

            //FileStream stream = new FileStream(path, FileMode.Open);
            //StreamReader sr = new StreamReader(stream);
            //var res = new Dictionary<string, Vozilo>();

            //  string outFile = HostingEnvironment.MapPath("~/App_Data/sort.txt");
            // Dictionary<string,Vozilo> contents = new Dictionary<string, Vozilo>();

            v = v.OrderBy(x => x.Value.Model).ToDictionary(x => x.Key, x => x.Value);

            //   UpdateCar(v);
            return v;
            //string[] tokens;
            // string line = "";

            //  foreach (var item in v)
            //  {
            //      // Treba nam if koji proverava status dal je false
            //      if (item.Equals(true))
            //      {
            //          contents.Add(item.Key,item.Value);

            //          // var sortedDict = from entry in contents orderby entry.Value ascending select entry;
            //          //contents = contents.OrderByDescending(x => x.Value.Sasija);

            //      }

            //  }

            //  foreach (KeyValuePair<string, Vozilo> item in v.OrderByDescending(key => key.Value))
            //  {

            //          contents.Add(item.Key, item.Value);

            //          // var sortedDict = from entry in contents orderby entry.Value ascending select entry;
            //          //contents = contents.OrderByDescending(x => x.Value.Sasija);


            //  }
            //// var ordered = contents.OrderBy(x => x.Value.Model);



            //res = contents.ToDictionary(x => string.Format("", x.Sasija), x => x);






            //string k = "";
            //foreach (var item in contents)
            //{
            //    // save user in file users.txt
            //    //Mazda|CX30D116|1|Crvena|4|Dizel|ZamenaNE|26550|true
            //    k += item.Marka + "|" + item.Model + "|" + item.Sasija + "|" +
            //        item.Boja + "|" + item.BrojVrata + "|" + item.VrstaGoriva + "|" + item.Description + "|" + item.Cena + "|"
            //        + string.Format(item.Status.ToString()).ToLower() + "\n";



            //}

            //File.WriteAllText(HostingEnvironment.MapPath("~/App_Data/sort.txt"), k);


        }
        public static Dictionary<string, Vozilo> SortDESCModel(Dictionary<string, Vozilo> v)
        {

            v = v.OrderByDescending(x => x.Value.Model).ToDictionary(x => x.Key, x => x.Value);

            //UpdateCar(v);


            return v;
            //string path = "~/App_Data/automobili.txt";
            //path = HostingEnvironment.MapPath(path);

            //FileStream stream = new FileStream(path, FileMode.Open);
            //StreamReader sr = new StreamReader(stream);
            //var res = new Dictionary<string, Vozilo>();

            //string outFile = HostingEnvironment.MapPath("~/App_Data/sort.txt");
            //List<Vozilo> contents = new List<Vozilo>();
            //string[] tokens;
            //string line = "";
            //while ((line = sr.ReadLine()) != null)
            //{

            //    tokens = line.Split('|');
            //    Vozilo v = new Vozilo(
            //        tokens[0], //mazda
            //        tokens[1], //cx
            //        tokens[2], //1
            //        tokens[3], //crvena
            //        int.Parse(tokens[4]),//4
            //        (Gorivo)Enum.Parse(typeof(Gorivo), tokens[5]),//dizel
            //        tokens[6],//zamena
            //        int.Parse(tokens[7]),//25660
            //        bool.Parse(tokens[8]));//true
            //    Treba nam if koji proverava status dal je false
            //   if (v.Status.Equals(true))
            //    {
            //        contents.Add(v);
            //    }
            //    contents = contents.OrderBy(x => x.Model).ToList();
            //    res = contents.ToDictionary(x => string.Format("", x.Sasija), x => x);

            //}



            //UpdateCar(res);

            //string k = "";
            //foreach (var item in contents)
            //{
            //    save user in file users.txt
            //    Mazda | CX30D116 | 1 | Crvena | 4 | Dizel | ZamenaNE | 26550 | true
            //    k += item.Marka + "|" + item.Model + "|" + item.Sasija + "|" +
            //        item.Boja + "|" + item.BrojVrata + "|" + item.VrstaGoriva + "|" + item.Description + "|" + item.Cena + "|"
            //        + string.Format(item.Status.ToString()).ToLower() + "\n";



            //}

            //File.WriteAllText(HostingEnvironment.MapPath("~/App_Data/sort.txt"), k);



        }

        public static Dictionary<string, Vozilo> SortMarkaDesc(Dictionary<string, Vozilo> v)
        {

            v = v.OrderByDescending(x => x.Value.Marka).ToDictionary(x => x.Key, x => x.Value);

            // UpdateCar(v);
            return v;

            //string path = "~/App_Data/automobili.txt";
            //path = HostingEnvironment.MapPath(path);

            //FileStream stream = new FileStream(path, FileMode.Open);
            //StreamReader sr = new StreamReader(stream);
            //var res = new Dictionary<string, Vozilo>();

            //string outFile = HostingEnvironment.MapPath("~/App_Data/sort.txt");
            //List<Vozilo> contents = new List<Vozilo>();
            //string[] tokens;
            //string line = "";
            //while ((line = sr.ReadLine()) != null)
            //{

            //    tokens = line.Split('|');
            //    Vozilo v = new Vozilo(
            //        tokens[0], //mazda
            //        tokens[1], //cx
            //        tokens[2], //1
            //        tokens[3], //crvena
            //        int.Parse(tokens[4]),//4
            //        (Gorivo)Enum.Parse(typeof(Gorivo), tokens[5]),//dizel
            //        tokens[6],//zamena
            //        int.Parse(tokens[7]),//25660
            //        bool.Parse(tokens[8]));//true
            //                               // Treba nam if koji proverava status dal je false
            //    if (v.Status.Equals(true))
            //    {
            //        contents.Add(v);
            //    }
            //    contents = contents.OrderByDescending(x => x.Marka).ToList();
            //    //res = contents.ToDictionary(x => string.Format("", x.Sasija), x => x);

            //}



            ////  UpdateCar(res);

            //string k = "";
            //foreach (var item in contents)
            //{
            //    // save user in file users.txt
            //    //Mazda|CX30D116|1|Crvena|4|Dizel|ZamenaNE|26550|true
            //    k += item.Marka + "|" + item.Model + "|" + item.Sasija + "|" +
            //        item.Boja + "|" + item.BrojVrata + "|" + item.VrstaGoriva + "|" + item.Description + "|" + item.Cena + "|"
            //        + string.Format(item.Status.ToString()).ToLower() + "\n";



            //}

            //File.WriteAllText(HostingEnvironment.MapPath("~/App_Data/sort.txt"), k);
        }

        public static Dictionary<string, Vozilo> sortMarkaAsc(Dictionary<string, Vozilo> v)
        {
            v = v.OrderBy(x => x.Value.Marka).ToDictionary(x => x.Key, x => x.Value);

            //UpdateCar(v);
            return v;
            //string path = "~/App_Data/automobili.txt";
            //path = HostingEnvironment.MapPath(path);

            //FileStream stream = new FileStream(path, FileMode.Open);
            //StreamReader sr = new StreamReader(stream);
            //var res = new Dictionary<string, Vozilo>();

            //string outFile = HostingEnvironment.MapPath("~/App_Data/sort.txt");
            //List<Vozilo> contents = new List<Vozilo>();
            //string[] tokens;
            //string line = "";
            //while ((line = sr.ReadLine()) != null)
            //{

            //    tokens = line.Split('|');
            //    Vozilo v = new Vozilo(
            //        tokens[0], //mazda
            //        tokens[1], //cx
            //        tokens[2], //1
            //        tokens[3], //crvena
            //        int.Parse(tokens[4]),//4
            //        (Gorivo)Enum.Parse(typeof(Gorivo), tokens[5]),//dizel
            //        tokens[6],//zamena
            //        int.Parse(tokens[7]),//25660
            //        bool.Parse(tokens[8]));//true
            //                               // Treba nam if koji proverava status dal je false
            //    if (v.Status.Equals(true))
            //    {
            //        contents.Add(v);
            //    }
            //    contents = contents.OrderBy(x => x.Marka).ToList();
            //    //res = contents.ToDictionary(x => string.Format("", x.Sasija), x => x);

            //}



            ////  UpdateCar(res);

            //string k = "";
            //foreach (var item in contents)
            //{
            //    // save user in file users.txt
            //    //Mazda|CX30D116|1|Crvena|4|Dizel|ZamenaNE|26550|true
            //    k += item.Marka + "|" + item.Model + "|" + item.Sasija + "|" +
            //        item.Boja + "|" + item.BrojVrata + "|" + item.VrstaGoriva + "|" + item.Description + "|" + item.Cena + "|"
            //        + string.Format(item.Status.ToString()).ToLower() + "\n";



            //}

            //File.WriteAllText(HostingEnvironment.MapPath("~/App_Data/sort.txt"), k);

        }
        public static Dictionary<string, Vozilo> SortCenaDesc(Dictionary<string, Vozilo> v)
        {
            v = v.OrderByDescending(x => x.Value.Cena).ToDictionary(x => x.Key, x => x.Value);

            // UpdateCar(v);
            return v;
            //string path = "~/App_Data/automobili.txt";
            //path = HostingEnvironment.MapPath(path);

            //FileStream stream = new FileStream(path, FileMode.Open);
            //StreamReader sr = new StreamReader(stream);
            //var res = new Dictionary<string, Vozilo>();

            //string outFile = HostingEnvironment.MapPath("~/App_Data/sort.txt");
            //List<Vozilo> contents = new List<Vozilo>();
            //string[] tokens;
            //string line = "";
            //while ((line = sr.ReadLine()) != null)
            //{

            //    tokens = line.Split('|');
            //    Vozilo v = new Vozilo(
            //        tokens[0], //mazda
            //        tokens[1], //cx
            //        tokens[2], //1
            //        tokens[3], //crvena
            //        int.Parse(tokens[4]),//4
            //        (Gorivo)Enum.Parse(typeof(Gorivo), tokens[5]),//dizel
            //        tokens[6],//zamena
            //        int.Parse(tokens[7]),//25660
            //        bool.Parse(tokens[8]));//true
            //                               // Treba nam if koji proverava status dal je false
            //    if (v.Status.Equals(true))
            //    {
            //        contents.Add(v);
            //    }
            //    contents = contents.OrderByDescending(x => x.Cena).ToList();
            //    //res = contents.ToDictionary(x => string.Format("", x.Sasija), x => x);

            //}



            ////  UpdateCar(res);

            //string k = "";
            //foreach (var item in contents)
            //{
            //    // save user in file users.txt
            //    //Mazda|CX30D116|1|Crvena|4|Dizel|ZamenaNE|26550|true
            //    k += item.Marka + "|" + item.Model + "|" + item.Sasija + "|" +
            //        item.Boja + "|" + item.BrojVrata + "|" + item.VrstaGoriva + "|" + item.Description + "|" + item.Cena + "|"
            //        + string.Format(item.Status.ToString()).ToLower() + "\n";



            //}

            //File.WriteAllText(HostingEnvironment.MapPath("~/App_Data/sort.txt"), k);
        }
        public static Dictionary<string, Vozilo> SortCenaAsc(Dictionary<string, Vozilo> v)
        {
            v = v.OrderBy(x => x.Value.Cena).ToDictionary(x => x.Key, x => x.Value);

            //UpdateCar(v);
            return v;

            //string path = "~/App_Data/automobili.txt";
            //path = HostingEnvironment.MapPath(path);

            //FileStream stream = new FileStream(path, FileMode.Open);
            //StreamReader sr = new StreamReader(stream);
            //var res = new Dictionary<string, Vozilo>();

            //string outFile = HostingEnvironment.MapPath("~/App_Data/sort.txt");
            //List<Vozilo> contents = new List<Vozilo>();
            //string[] tokens;
            //string line = "";
            //while ((line = sr.ReadLine()) != null)
            //{

            //    tokens = line.Split('|');
            //    Vozilo v = new Vozilo(
            //        tokens[0], //mazda
            //        tokens[1], //cx
            //        tokens[2], //1
            //        tokens[3], //crvena
            //        int.Parse(tokens[4]),//4
            //        (Gorivo)Enum.Parse(typeof(Gorivo), tokens[5]),//dizel
            //        tokens[6],//zamena
            //        int.Parse(tokens[7]),//25660
            //        bool.Parse(tokens[8]));//true
            //                               // Treba nam if koji proverava status dal je false
            //    if (v.Status.Equals(true))
            //    {
            //        contents.Add(v);
            //    }
            //    contents = contents.OrderBy(x => x.Cena).ToList();
            //    //res = contents.ToDictionary(x => string.Format("", x.Sasija), x => x);

            //}



            ////  UpdateCar(res);

            //string k = "";
            //foreach (var item in contents)
            //{
            //    // save user in file users.txt
            //    //Mazda|CX30D116|1|Crvena|4|Dizel|ZamenaNE|26550|true
            //    k += item.Marka + "|" + item.Model + "|" + item.Sasija + "|" +
            //        item.Boja + "|" + item.BrojVrata + "|" + item.VrstaGoriva + "|" + item.Description + "|" + item.Cena + "|"
            //        + string.Format(item.Status.ToString()).ToLower() + "\n";



            //}

            //File.WriteAllText(HostingEnvironment.MapPath("~/App_Data/sort.txt"), k);
        }
        #endregion
        public static Boolean isAlphaNumeric(string strToCheck)
        {
            Regex rg = new Regex(@"[a-zA-Z0-9]*");
            return rg.IsMatch(strToCheck);
        }

        public static void UpdateCar(Dictionary<string, Vozilo> vozila)
        {
            string k = "";
            foreach (var item in vozila)
            {
                // save user in file users.txt
                //Mazda|CX30D116|1|Crvena|4|Dizel|ZamenaNE|26550|true
                k += item.Value.Marka + "|" + item.Value.Model + "|" + item.Value.Sasija + "|" +
                    item.Value.Boja + "|" + item.Value.BrojVrata + "|" + item.Value.VrstaGoriva + "|" + item.Value.Description + "|" + item.Value.Cena + "|"
                    + string.Format(item.Value.Status.ToString()).ToLower() + "|" + item.Value.Korisnik + "\n";



            }
            File.WriteAllText(HostingEnvironment.MapPath("~/App_Data/automobili.txt"), k);

        }
        public static Dictionary<string, Vozilo> Range(Dictionary<string, Vozilo> v,int min,int max)
        {
            Dictionary<string, Vozilo> res = new Dictionary<string, Vozilo>();
            foreach (var item in v)
            {
                if (item.Value.Cena>=min && item.Value.Cena<=max)
                {
                    res.Add(item.Key, item.Value);
                }
            }
          

           
            return res;

        }
        public static Dictionary<string, Vozilo> Search(Dictionary<string, Vozilo> v,string search,string itemOfSearch)
        {
            Dictionary<string, Vozilo> res = new Dictionary<string, Vozilo>();

            foreach (var item in v)
            {
                if (itemOfSearch.Equals("Model"))
                {
                    if (item.Value.Model.Equals(search))
                    {
                        res.Add(item.Key, item.Value);
                    }
                }
                else if (itemOfSearch.Equals("Brand Name"))
                {
                    if (item.Value.Marka.Equals(search))
                    {
                        res.Add(item.Key, item.Value);
                    }
                }
            }



            return res;

        }
    }
  }
