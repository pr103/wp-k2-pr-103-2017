﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace WebApplication1.Models
{
    public class Korisnik
    {

        private Korisnik_Uloga uloga;

        public Korisnik_Uloga Uloga
        {
            get { return uloga; }
            set { uloga = value; }
        }


        [Required]
        [Display(Name ="username")]
        [StringLength(60,MinimumLength=3,ErrorMessage ="Username must be longer that 3 character!")]
        [DataType(DataType.Text)]
        private string username;

        // Korisničko ime(jedinstveno i minimalno 3 karaktera)
        public string Username
        {
            get { return username; }
            set { username = value; }
        }

        [Required]
        [Display(Name = "password")]
        [StringLength(60, MinimumLength = 8,ErrorMessage ="Password must be lenght <=8 character!")]
        [DataType(DataType.Password)]
        private string password;

        //Lozinka(minimalno 8 karaktera koji mogu biti slova i brojevi)
        public string Password
        {
            get { return password; }
            set { password = value; }
        }

    

        private string name;

        public string Name
        {
            get { return name; }
            set { name = value; }
        }

        private string lastname;

        public string Lastname
        {
            get { return lastname; }
            set { lastname = value; }
        }

        private string gender;

        public string Gender
        {
            get { return gender; }
            set { gender = value; }
        }

        private string email;

        public string Email
        {
            get { return email; }
            set { email = value; }
        }

        private string date;

        public string Date
        {
            get { return date; }
            set { date = value; }
        }

        private bool loggedIn;

        public bool LoggedIn
        {
            get { return loggedIn; }
            set { loggedIn = value; }
        }

        public Korisnik(string username, string password, string name, string lastname, string date,string email,string gender,Korisnik_Uloga uloga)
        {
         
            this.username = username;
            this.password = password;
            this.name = name;
            this.lastname = lastname;
            this.gender = gender;
            this.email = email;
            this.date = date;
            this.uloga = uloga;
            LoggedIn = true;
        }

        public Korisnik()
        {
            username = "";
            password = "";
            LoggedIn = false;

        }

       
    }
}