﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using WebApplication1.Models;

namespace WebApplication1
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);


            // Učitamo proizvode iz datoteke u memoriju
            Dictionary<string, Vozilo> vozila = Baza.ReadCars("~/App_Data/automobili.txt");
            HttpContext.Current.Application["Vozila"] = vozila;

            // Učitamo proizvode iz datoteke u memoriju
            Dictionary<string, Korisnik> korisnici = Baza.ReadUsers("~/App_Data/korisnici.txt");
            HttpContext.Current.Application["Korisnici"] = korisnici;


            // Učitamo proizvode iz datoteke u memoriju
           Dictionary<string, Kupovina> korpa = Baza.ReadItems("~/App_Data/kupacKupovine.txt");
           HttpContext.Current.Application["Korpa"] = korpa;
        }
    }
}
